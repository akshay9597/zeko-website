var express = require('express');
var router = express.Router();
var Parse = require('parse/node');

Parse.initialize("kcAt6RkWQy2Iqzoq83iH9aEL93pFhxq7Ox9cu1PT", "ORXiYhOkkvkAjrQUneYKrxTa0zeETuRXxY9UiMQW" /*, "uVJMei2T5XiMlrJGFvqnHjKLobj2e7zPLtCxZ36a" */);


/* GET users listing. */
router.get('/', function(req, res, next) {
	Parse.User.enableUnsafeCurrentUser();
	Parse.User.become(req.cookies.sessionToken).then(function (user) {
		console.log(user);
		if (Parse.User.current() == null){
		res.redirect("/account/login");
		return;
	}

  	res.send('respond with a resource');
  }, function(error) {
  	res.send(error);
  });

	
});

router.get('/checkAuth', function(req, res, next) {
	Parse.User.enableUnsafeCurrentUser();
	Parse.User.become(req.cookies.sessionToken).then(function (user) {
		if (Parse.User.current() == null){
			res.json({loggedIn: false});
			return;
		}

  	res.json({loggedIn: true, name: user.get("name")});
  }, function(error) {
  	res.json({loggedIn: false});
  });

	
});

router.get(['/login','/register'], function(req, res, next) {
	res.render("login")
});

router.post('/login', function(req, res, next) {
  	Parse.User.logIn(req.body.username, req.body.password, {
		success: function(user) {
			console.log(user.get("sessionToken"));
			res.cookie("sessionToken", user.get("sessionToken"));
			res.redirect("/");
			//res.send("Login Successfull");
		},
		error: function(user, error) {
			res.send("Error: " + error.code + " " + error.message);
		}
	});
});

router.post('/register', function(req, res, next) {
  
	var user = new Parse.User();
	user.set("username", req.body.email);
	user.set("password", req.body.password);
	user.set("email", req.body.email);
	user.set("name", req.body.name);

	user.signUp(null, {
		success: function(user) {
			console.log(user);
			res.cookie("sessionToken", user.get("sessionToken"));
			res.redirect("/");
		},
		error: function(user, error) {
			res.send("Error: " + error.code + " " + error.message);
		}
	});

});

router.get('/logout', function(req, res, next) {
	//Parse.User.logOut();
	res.clearCookie('sessionToken');
	res.redirect("/");
  	//res.send('respond with a resource');
});

module.exports = router;
