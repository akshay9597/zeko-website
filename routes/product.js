var express = require('express');
var router = express.Router();
var Parse = require('parse/node');

Parse.initialize("kcAt6RkWQy2Iqzoq83iH9aEL93pFhxq7Ox9cu1PT", "ORXiYhOkkvkAjrQUneYKrxTa0zeETuRXxY9UiMQW" /*, "uVJMei2T5XiMlrJGFvqnHjKLobj2e7zPLtCxZ36a" */);

/* GET Product listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get(['/:id-*', '/:id'], function(req, res, next) {
	var productId = req.params.id;

	var productObject = Parse.Object.extend("Product");
	var query = new Parse.Query(productObject);
	query.equalTo("productId", productId);
	query.equalTo("isListed", true);
	query.include("designerPointer");
	query.include("productImageId");
	query.first({
		success: function (product) {

			if (typeof product == 'undefined')
				return res.send("Object Not Found");

			designer = product.get("designerPointer");
			console.log(product);
			var productArray = {
				id : productId,
				name : product.get("productName"),
				nameDashed : product.get("productName").replace(new RegExp(' ', 'g'), "-"),
				size : product.get("size"),
				variable : product.get("size").length > 1,
				maxPrice : product.get("maxPrice"),
				price : product.get("price"),
				likes : product.get("likes"),
				description : product.get("productDescription"),
				designer: {
					id : designer.get("designerId"),
					name : designer.get("designerName"),
					nameDashed : designer.get("designerName").replace(new RegExp(' ', 'g'), "-"),
					address : designer.get("designerAddress"),
					returnPolicy : designer.get("returnPolicy"),
				}
			}
			var productImages = product.get("productImageId");
			productArray.imageUrls = [];
			for (var i = 0; i < productImages.length; i++) {
				productArray.imageUrls.push(productImages[i].get("productImageFile").url());
			};
			res.render('single_product', {title: productArray.name+" by "+productArray.designer.name, product: productArray});
		},
		error: function (error) {
			res.send("Error: " + error.code + " " + error.message);
		}
	});
});

module.exports = router;