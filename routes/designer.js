var express = require('express');
var router = express.Router();
var Parse = require('parse/node');
var async = require('async');

Parse.initialize("kcAt6RkWQy2Iqzoq83iH9aEL93pFhxq7Ox9cu1PT", "ORXiYhOkkvkAjrQUneYKrxTa0zeETuRXxY9UiMQW" /*, "uVJMei2T5XiMlrJGFvqnHjKLobj2e7zPLtCxZ36a" */);

/* GET Product listing. */
router.get('/', function(req, res, next) {

	var designerObject = Parse.Object.extend("Designer");
	var query = new Parse.Query(designerObject);
	query.equalTo("isListed", true);
	query.find({
		success: function (designers) {

			if (typeof designers == 'undefined'){
				return console.log(true, "Error: Object Not Found");
			}

			var designerArray = [];
			for (var i = 0; i < designers.length ; i++) {
				var designer = designers[i];
				var tempArray = {
					id : designer.get("designerId"),
					name : designer.get("designerName"),
					nameDashed : designer.get("designerName").replace(new RegExp(' ', 'g'), '-'),
					image : designer.get("designerLogo").url()
					// address : designer.get("originalAddress"),
					// altAddress : designer.get("alternateAddress"),
				};
				designerArray.push(tempArray); 
			};
			res.render("designers", {title: "Designers", designers : designerArray});
		},
		error: function (error) {
			return console.log(true, "Error: " + error.code + " " + error.message);
		}
	});
});

router.get(['/:id-*', '/:id'], function(req, res, next) {
	var designerId = req.params.id;
	var page = req.query.page || 1 ;
	var ITEMS_PER_PAGE = 12;

	async.parallel([
		function (callback) {
			var designerObject = Parse.Object.extend("Designer");
			var query = new Parse.Query(designerObject);
			query.equalTo("designerId", designerId);
			query.equalTo("isListed", true);
			query.first({
				success: function (designer) {

					if (typeof designer == 'undefined'){
						return callback(true, "Error: Object Not Found");
					}

					callback(null, designer);
				},
				error: function (error) {
					return callback(true, "Error: " + error.code + " " + error.message);
				}
			});
		}, 
		function (callback) {

			var productObject = Parse.Object.extend("Product");
			var query = new Parse.Query(productObject);
			query.startsWith("productId", designerId);
			query.equalTo("isListed", true);
			query.limit(ITEMS_PER_PAGE);
			query.skip((page - 1) * ITEMS_PER_PAGE);
			query.include("productImageId");
			query.find({
				success: function (products) {

					if (typeof products == 'undefined' || products.length == 0)
						return callback(true, "Error: Object Not Found");

					callback(null, products);
				},
				error: function (error) {
					return callback(error, "Error: " + error.code + " " + error.message);
				}
			});
		}, function (callback) {
			var productObject = Parse.Object.extend("Product");
			var query = new Parse.Query(productObject);
			query.startsWith("productId", designerId);
			query.equalTo("isListed", true);

			query.count().then(function(result) { callback(null, result) })
		}
	],
	function (error, results) {
		if(error != null){
			res.send("Something Went Wrong !");
			return console.log(error, results);
		}

		var designer = results[0];
		var products = results[1];
		var pages = Math.floor(results[2] / ITEMS_PER_PAGE);
		var pageArray = [];
		pageArray[page] = "current"; 

		var designerArray = {
			id : designer.get("designerId"),
			name : designer.get("designerName"),
			nameDashed : designer.get("designerName").replace(new RegExp(' ', 'g'), '-'),
			image : designer.get("designerLogo").url(),
			imageCover : designer.get("designerCoverImage").url(),
			address : designer.get("designerAddress")
		};

		async.map(products, function (product, callback){
			var tempArray = {
				id : product.get("productId"),
				name : product.get("productName"),
				nameDashed : product.get("productName").replace(new RegExp(' ', 'g'), "-"),
				size : product.get("size"),
				variable : product.get("size").length > 1,
				maxPrice : product.get("maxPrice"),
				price : product.get("price"),
				likes : product.get("likes"),
				description : product.get("productDescription")
			};

			var productImageId = product.get("productImageId");
			tempArray.image = [];
			tempArray.image.push(productImageId[0].get("productImageFile").url());
			tempArray.image.push((typeof productImageId[1] != 'undefined') ? productImageId[1].get("productImageFile").url() : tempArray.image[0]);
			
			return callback(null, tempArray);
			
		}, function (error, products) {
			if(error){
				return console.log(error);
			}
			res.render("designer_products", {title: designer.get("designerName"),designer : designerArray, products: products, pages: pages+1, page: pageArray});

		});
		
		// var productArray = [];
		// for (var i = products.length - 1; i >= 0; i--) {
		// 	var product = products[i];
		// 	productArray.push({
		// 		id : product.get("productId"),
		// 		name : product.get("productName"),
		// 		nameDashed : product.get("productName").replace(new RegExp(' ', 'g'), '-'),
		// 		price : product.get("price"),
		// 		likes : product.get("likes"),
		// 	});
		// };
		// console.log(results);
	});
});


function getFeed (products, productArray, id, callback) {
	if (typeof products[id] == 'undefined'){
		callback(productArray);
		return;
	}
	console.log("Doing "+ id);
	var product = products[id].get("productPointer");
	var designer = product.get("designerPointer");
	var tempArray = {
		id : product.get("productId"),
		name : product.get("productName"),
		nameDashed : product.get("productName").replace(new RegExp(' ', 'g'), "-"),
		size : product.get("size"),
		variable : product.get("size").length > 1,
		maxPrice : product.get("maxPrice"),
		price : product.get("price"),
		likes : product.get("likes"),
		description : product.get("productDescription")
	};
	var productImageId = product.get("productImageId");
	var tempArray = [];
	for (var i = 0; i < productImageId.length && i < 2; i++) {
		tempArray.push(productImageId[i].objectId || productImageId[i].id);
	};

	var productImageObject = Parse.Object.extend("ProductImage");
	var query = new Parse.Query(productImageObject);
	query.containedIn("objectId", tempArray);
	query.find({
	  success: function(productImages) {
	  	tempArray.image = [];
	  	for (var i = 0; i < productImages.length; i++) {
	  		tempArray.image.push(productImages[i].get("productImageFile").url());
	  	};
	    tempArray.image = productImage.get("productImageFile").url();
	    productArray.push(tempArray);
	    console.log(tempArray);
	    getFeed(products, productArray, id+1, callback);
	  },
	  error: function(object, error) {
	    // The object was not retrieved successfully.
	    // error is a Parse.Error with an error code and message.
	  }
	});
	
}

module.exports = router;