var express = require('express');
var router = express.Router();
var Parse = require('parse/node');
var async = require('async');
var NodeCache = require( "node-cache" );
var myCache = new NodeCache();

Parse.initialize("kcAt6RkWQy2Iqzoq83iH9aEL93pFhxq7Ox9cu1PT", "ORXiYhOkkvkAjrQUneYKrxTa0zeETuRXxY9UiMQW" /*, "uVJMei2T5XiMlrJGFvqnHjKLobj2e7zPLtCxZ36a" */);

/* GET home page. */
router.get('/', function(req, res, next) {

  	var feedObject = Parse.Object.extend("Feed");
	var query = new Parse.Query(feedObject);
	//query.equalTo("productPointer.isListed", true); //Not supported. needs fixing while generating output
	query.equalTo("isListed", true);
	query.include("productPointer");
	query.include("productPointer.productImageId");
	//query.include("productPointer.designerPointer");
	query.find({
		success: function (products) {

			if (typeof products == 'undefined')
				return res.send("Object Not Found");
			
			var productArray = myCache.get( "index.productArray" );
			if (productArray == undefined){
				async.map(products, getProduct, function (err, productArray) {
					if(err == null){
						myCache.set( "index.productArray", productArray, 300 ); 
						res.render("index", {title: "Home", products: productArray});
					}
				});
			} else {
				res.render("index", {title: "Home", products: productArray});
			}
		},
		error: function (error) {
			res.send("Error: " + error.code + " " + error.message);
		}
	});
});

function getProduct (product, callback) {
	var product = product.get("productPointer");
	var designer = product.get("designerPointer");
	var tempArray = {
		id : product.get("productId"),
		name : product.get("productName"),
		nameDashed : product.get("productName").replace(new RegExp(' ', 'g'), "-"),
		size : product.get("size"),
		variable : product.get("size").length > 1,
		maxPrice : product.get("maxPrice"),
		price : product.get("price"),
		likes : product.get("likes"),
		description : product.get("productDescription"),
		image: product.get("productImageId")[0].get("productImageFile").url(),
		imagebg: typeof  product.get("productImageId")[1] != 'undefined' ? product.get("productImageId")[1].get("productImageFile").url():product.get("productImageId")[0].get("productImageFile").url(),
		designer: {
			id : designer.get("designerId"),
			name : designer.get("designerName"),
			address : designer.get("designerAddress"),
			returnPolicy : designer.get("returnPolicy"),
		}
	};
	return callback(null, tempArray);
}

module.exports = router;
