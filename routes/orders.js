var express = require('express');
var router = express.Router();
var Parse = require('parse/node');
var async = require('async');
//var bodyParser = require('body-parser');

Parse.initialize("kcAt6RkWQy2Iqzoq83iH9aEL93pFhxq7Ox9cu1PT", "ORXiYhOkkvkAjrQUneYKrxTa0zeETuRXxY9UiMQW" /*, "uVJMei2T5XiMlrJGFvqnHjKLobj2e7zPLtCxZ36a" */);

router.post('/checkout', isAuthenticated, function(req, res, next) {

	var productId = req.body.productId;
	console.log(req.body.quantity);
	var quantity = req.body.quantity || 1;
	var size = req.body.size || "Original Size";

	async.parallel([
	function (callback) {
		var productObject = Parse.Object.extend("Product");
		var query = new Parse.Query(productObject);
		query.equalTo("productId", productId);
		query.equalTo("isListed", true);
		query.first({
			success: function (product) {

				if (typeof product == 'undefined')
					return callback("error", "Object Not Found");

				var productArray = {
					oid: product.id,
					id : productId,
					quantity : quantity,
					name : product.get("productName"),
					size : size,
					maxPrice : product.get("maxPrice"),
					price : product.get("price")*quantity
				};

				
				return callback(null, productArray);
				
			},
			error: function (error) {
				callback("Error", "Error: " + error.code + " " + error.message);
			}
		});
	},
	function(callback) {

		var addressPointer = Parse.User.current().get("addressPointer");
		if (addressPointer != null) {
			var addresses = [];
			for (var i = addressPointer.length - 1; i >= 0; i--) {
				addresses.push(addressPointer[i].objectId || addressPointer[i].id );
			};
			console.log(addresses);
			var addressObject = Parse.Object.extend("UserAddress");		
			var query = new Parse.Query(addressObject);
			query.containedIn("objectId", addresses);
			query.first({
				success: function  (results) {
					if(results == null)
						return callback(null, false);

					var address = results;
					var addressArray = {
						id: address.id,
						name: Parse.User.current().get("name"),
						email: Parse.User.current().get("email"),
						city: address.get("city"),
						house: address.get("house"),
						locality: address.get("locality"),
						phone: address.get("phoneNumber"),
						pincode: address.get("pincode")
					}
					return callback(null, addressArray);
				},
				error: function(error) {
					return callback("error", error);
				}
			});
		} else {
			return callback(null, false);
		}

	}],
	function(err, results) {
		if (err == null) {
			res.render("checkout", {title: "Checkout", product: results[0], info: results[1] });
		} else {
			console.log(err, results);
		}
	});

});

router.post('/place', isAuthenticated, function(req, res, next) {
	var productObject = Parse.Object.extend("Product");
	var addressObject = Parse.Object.extend("UserAddress");
	var orderObject = Parse.Object.extend("Order");
	var configObject = Parse.Object.extend("Config");

	var product = new productObject;
	product.id = req.body.productPointer;

	

	var outputArray = {
		address: {
			city: req.body.billing_city,
			house: req.body.billing_address_1,
			locality: req.body.billing_address_2,
			name: req.body.billing_name,
			email: Parse.User.current().get("email"),
			phoneNumber: req.body.billing_phone,
			pincode: req.body.billing_postcode
		},
		order: {
			netPrice: parseInt(req.body.price),
			totalPrice: parseInt(req.body.price),
			orderTime: new Date().toDateString(),
			orderStatus: "Ordered",
			userPointer: Parse.User.current(),
			quantity: parseInt(req.body.quantity)
		}
	};

	//res.render("checkout_success",{title: "Order Placed", address: outputArray.address, order: outputArray.order});

	async.parallel([
		function(parallelCallback) {
			async.waterfall([
			    function(callback) {
			    	var query = new Parse.Query(addressObject);
			    	query.get(req.body.addressPointer, {
						success: function(address) {
							callback(null, address);
						},
						error: callback
					});
			        
			    },
			    function(address, callback) {
			      	
					if (address.get("city") != req.body.billing_city ||
						address.get("house") != req.body.billing_address_1 ||
						address.get("locality") != req.body.billing_address_2 ||
						address.get("name") != req.body.billing_name ||
						address.get("phoneNumber") != req.body.billing_phone ||
						address.get("pincode") != req.body.billing_postcode	
					) {
						// console.log(address.get("city") != req.body.billing_city , 
						// address.get("house") != req.body.billing_address_1 , 
						// address.get("locality") != req.body.billing_address_2 , 
						// address.get("name") != req.body.billing_name , 
						// address.get("phoneNumber") != req.body.billing_phone , 
						// address.get("pincode") != req.body.billing_postcode	);
						// callback("error", "error");
						var newAddress = new addressObject;
						newAddress.set("city", req.body.billing_city);
						newAddress.set("house", req.body.billing_address_1);
						newAddress.set("locality", req.body.billing_address_2);
						newAddress.set("name", req.body.billing_name);
						newAddress.set("phoneNumber", req.body.billing_phone);
						newAddress.set("pincode", req.body.billing_postcode);
						newAddress.save(null, {
							success: function(address) {
								callback(null, address);
							},
							error: callback
						});
					} else {
						callback(null, address);
					}
			    },
			    function(address, callback) {
			    	var query = new Parse.Query(configObject);
					query.get("hJvPmAaqld", {  // "hJvPmAaqld" is objectId for parameter=orderId
						success: function(config) {
							var orderId = config.get("counter");
							outputArray.order.orderId = orderId + 1;
							config.increment("counter");
							config.save();
							callback(null, address, orderId);
						}
					});
			    },
			    function(address, orderId, callback) {
			        var order = new orderObject;
					console.log(Parse.User.current());

					order.set("orderId", orderId + 1);
					order.set("netPrice", parseInt(req.body.price));
					order.set("totalPrice", parseInt(req.body.price));
					order.set("orderStatus", "Ordered");
					order.set("productPointer", product);
					order.set("userAddressPointer", address);
					order.set("userPointer", Parse.User.current());
					order.set("quantity", parseInt(req.body.quantity));
					order.save(null, {
						success: function (order) {
							callback(null, order, address);
						}
					});
			    },
			    function(order, address, callback) {
			    	var user = Parse.User.current();
					user.add("addressPointer", address);
					user.add("orders", order);
					user.save();
					callback(null, "success");
			    }
			], function(err, results) {
				if (err == null) {
					parallelCallback(null, "success");
				};
			});
		},
		function(parallelCallback) {
			var query = new Parse.Query(productObject);
			query.get(req.body.productPointer, {
				success: function(product) {
					outputArray.product = {
						id: product.get("productId"),
						name: product.get("productName")
					};
					parallelCallback(null, product);
				},
				error: function(object, error) {
					parallelCallback("error", error);
				}
			});
		}
	],
	function(err, results) {
		if(err == null){
			outputArray.title = "Order Placed";
			console.log(outputArray);
			res.render("checkout_success", outputArray);
		} else {
			console.log(err, results);
		}
	});


	
});

function isAuthenticated(req, res, next) {

    Parse.User.enableUnsafeCurrentUser();
	Parse.User.become(req.cookies.sessionToken).then(function (user) {
		if (Parse.User.current() == null){
			res.redirect('/account/login');
			return;
		}

  	next();
  }, function(error) {
  	res.redirect('/account/login');  });
}

module.exports = router;